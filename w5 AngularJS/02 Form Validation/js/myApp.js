var app = angular.module("myApp", []);
app.controller("myCtrl", function($scope) {
  $scope.scores = ["5", "4", "3", "2", "1", "0"];
  $scope.score = "0";
  $scope.payments = ["Visa", "Paypal", "Mastercard"];
  $scope.payment = "Visa";
  $scope.code = "Type Student's Code Here";
});