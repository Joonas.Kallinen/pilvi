// koodi:

function Stuff(name, weight) {
    this.name = name;
    this.weight = weight;
}

class Bag {
    constructor(maxweight) {
        this.maxweight = maxweight;
        this.items = [];
        this.w = 0;
    }
    add(x) {
        //console.log(x);
        if (x instanceof Stuff) {
            if (this.items.indexOf(x) == -1) {
                if ((this.w+x.weight) <= this.maxweight) {
                    this.w += x.weight;
                    this.items.push(x);
                } else {
                    console.log("Error: " + x.name + " is too heavy ("+x.weight+").");
                }
            } else {
                console.log("Error: " + x.name + " already in a bag.");
            }
        } else {
            console.log("Error: Cannot add wrong type of item.");
        }
    }
    weight() {
        var sum = 0;
        for (var i = 0; i < this.items.length; i++) {
            sum += this.items[i].weight;
        }
        return sum;
    }
}

class Cargo {
    constructor(maxweight) {
        this.maxweight = maxweight;
        this.w = 0;
        this.items = [];
    }
    add(x) {
        if (x instanceof Bag) {
                if ((this.w+x.weight()) <= this.maxweight) {
                    this.w += x.weight();
                    this.items.push(x);
                } else {
                    console.log("Error: " + x.constructor.name + " is too heavy ("+x.weight()+").");
                }
            } else {
                console.log("Error: Cannot add " + x.name + " to cargo.");
            }
    }
    weight() {
        var sum = 0;
        for (var i = 0; i < this.items.length; i++) {
            sum += this.items[i].weight();
        }
        return sum;
    }
}    
    
var stone = new Stuff("stone", 3);
var book = new Stuff("book", 7);
var cotton = new Stuff("cotton", 0.001);

var bag = new Bag(10);
var vuitton = new Bag(3);

var schenker = new Cargo(15);


bag.add(stone);
console.log("laukun paino, pitäisi olla 3: " + bag.weight());
bag.add(stone); // virhe: "Stuff lisätty jo, ei onnistu!"

bag.add(book);
console.log("laukun paino, pitäisi olla 10: " + bag.weight());

bag.add(cotton); // virhe: "Liian painava, ei pysty!"

console.log("laukun paino, pitäisi olla 10: " + bag.weight());


schenker.add(bag);
schenker.add(cotton); // virhe: Vääränlainen esine, ei onnistu!

console.log("Ruuman paino, pitäisi olla 10: " + schenker.weight());

vuitton.add(cotton);
schenker.add(vuitton);
console.log("Ruuman paino, pitäisi olla noin 10.001: " + schenker.weight()); 

cotton.weight = 300;
console.log("Ruuman paino, pitäisi olla 310: " + schenker.weight()); // hups!
