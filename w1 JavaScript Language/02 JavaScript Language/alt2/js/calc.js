function getNumber(id) {
    return parseInt(document.getElementById(id).value);
}

function setResult(result) {
    document.getElementById("result").innerHTML = result;
}

function plus() {
    setResult(getNumber("firstValue") + getNumber("secondValue"));
}

function mult() {
    setResult(getNumber("firstValue") * getNumber("secondValue"));
}

function subtract() {
    setResult(getNumber("firstValue") - getNumber("secondValue"));
}

function divide() {
    var result = getNumber("firstValue") / getNumber("secondValue");
    if (getNumber("secondValue") === 0) {
        setResult("Cannot divide by zero.");
    } else { 
        setResult(result);
    }
}