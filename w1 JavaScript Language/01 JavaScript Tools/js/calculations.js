function multiplyBy()
{
// check the input
var firstNumber = parseFloat(document.getElementById("firstNumber").value);
var secondNumber = parseFloat(document.getElementById("secondNumber").value);

var result = firstNumber * secondNumber;
if (isNaN(result)) {
    document.getElementById("result").innerHTML = "Not a number.";
} else {
    document.getElementById("result").innerHTML = result;
}

}

function divideBy() 
{ 
// check the input
var firstNumber = parseFloat(document.getElementById("firstNumber").value);
var secondNumber = parseFloat(document.getElementById("secondNumber").value);

var result = firstNumber / secondNumber;
if (isNaN(result)) {
    document.getElementById("result").innerHTML = "Not a number.";
} else {
    if (secondNumber === 0) {
        document.getElementById("result").innerHTML = "Cannot divide by zero.";
    } else { 
        document.getElementById("result").innerHTML = result;
    }
}

}