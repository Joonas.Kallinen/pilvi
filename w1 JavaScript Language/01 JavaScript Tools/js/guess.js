let targetNumber = parseInt(Math.floor(Math.random() * 10) + 1);
var tries = 5;
function init () {
	console.log("You have to code this!");
	var form = document.getElementById("form");
	form.addEventListener("submit", function(event){
	    event.preventDefault();
	    var number = parseInt(document.getElementById("number"));
	    check(number);
	});
}

function check (value) {
    if (tries > 0) {
        if(value === targetNumber) {
            showWin();
        } else {
            showError();
        }
        tries--;
    } else {
        showLoss();
    }
}

function showWin () {
    console.log("You won!");
    document.getElementById("message").innerHTML = "You won!";
    document.getElementById("form").style.display = "none";
}

function showError () {
    console.log("...try again. Tries left: " + tries);
    document.getElementById("message").innerHTML = "...try again. Tries left: " + tries;
}

function showLoss () {
    console.log("You lost!");
    document.getElementById("message").innerHTML = "You lost!";
    document.getElementById("form").style.display = "none";
}

init();
