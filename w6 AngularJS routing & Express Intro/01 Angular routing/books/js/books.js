/*global angular  */

/* we 'inject' the ngRoute module into our app. This makes the routing functionality to be available to our app. */
var myApp = angular.module('myApp', ['ngRoute'])

/* the config function takes an array. */
myApp.config( ['$routeProvider', function($routeProvider) {
  $routeProvider
    .when('/search', {
		  templateUrl: 'templates/search.html',
      controller: 'searchController'
		})
    .when('/detail/:id', {
      templateUrl: 'templates/detail.html',
      controller: 'detailController'
    })
    .when('/favourites', {
		  templateUrl: 'templates/favourites.html',
      controller: 'favouritesController'
		})
// TODO 1
    .when('/recent', {
		  templateUrl: 'templates/recent.html',
      controller: 'recentController'
		})
		.otherwise({
		  redirectTo: 'search'
		})
	}])


myApp.controller('searchController', function($scope, $http) {
  $scope.message = 'This is the search screen'
  $scope.search = function($event) {
    console.log('search()')
    if ($event.which == 13) { // enter key presses
      var search = $scope.searchTerm
      console.log(search)
      var url = 'https://www.googleapis.com/books/v1/volumes?maxResults=40&fields=items(id,volumeInfo(title))&q='+search
      $http.get(url).success(function(response) {
        console.log(response)
        $scope.books = response.items
        $scope.searchTerm = ''
      })
    }
  }
})

myApp.controller('detailController', function($scope, $routeParams, $http) {
  $scope.message = 'This is the detail screen'
  $scope.id = $routeParams.id
  $scope.addToFavourites = function(id) {
    console.log('adding: '+id+' to favourites.')
    var book;
    var url = 'https://www.googleapis.com/books/v1/volumes?maxResults=40&fields=items(id,volumeInfo)&q='+id;
    $http.get(url).success(function(response) {
        var item = response.items[0];
        book = {
          bookId: item.id,
          bookTitle: item.volumeInfo.title,
          bookAuthor: item.volumeInfo.authors, 
          thumbnailLink: item.volumeInfo.imageLinks.smallThumbnail
        };
    var books = Array();
    if (!localStorage.getItem('books')) {
      books.push(book);
      var items = JSON.stringify(books);
      localStorage.setItem('books',items);
    } else {
      var items = localStorage.getItem('books');
      books = JSON.parse(items);
      if (items.indexOf(book) == -1) {
        books.push(book);
      }
      items = JSON.stringify(books);
      localStorage.setItem('books',items);
    }
      })
  }
})
 
myApp.controller('favouritesController', function($scope) {
  console.log('fav controller')
  $scope.message = 'This is the favourites screen'
  $scope.delete = function(id) {
    console.log('deleting id '+id)
    //localStorage.removeItem(id, id)
    
    if (localStorage.getItem('books')) {
      var items = localStorage.getItem('books');
      books = JSON.parse(items);
      for (var i = 0; i < books.length; i++) {
        if (books[i].bookId == id) {
          books.splice(books.indexOf(books[i]),1);
        }
      }
      items = JSON.stringify(books);
      localStorage.setItem('books',items);
    }
    init()
  }
  var init = function() {
    console.log('getting books')
    var books = localStorage.getItem('books');
    console.log(books);
    items = JSON.parse(books);
    /*
    for (var a in localStorage) {
      items.push(localStorage[a])
    }
    */
    console.log(items)
    if (items != null) {
      $scope.books = items
    } else {
      $scope.books = null;
    }
  }
  init()
})

// TODO 1
myApp.controller('recentController', function($scope) {
  $scope.message = 'This is the recent screen'
})
